public with sharing class NG_HomePageController 
{
 // demo to Vaishali
   public  List<Account> accountList{get;set;}
   
   //List<Account> accountList = new List<Account>();
   public NG_HomePageController()
   {
    //List<Account> accountList = new List<Account>();
    accountList  =  [ Select  Name,
                                 Nature_of_Business__c,
                                 Tender_Stage__c,
                                 Pending_With__c,
                                 Registered_Number__c,
                                 Registered_Address_Line_1__c,
                                 Registered_Address_Line_2__c,
                                 Registered_Address_Line_3__c,
                                 Commercial_Contact_Name__c,
                                 Dispatch_Email_Address__c,
                                 Offer_Stage__c,
                                 Offer_Status__c
                                 FROM Account
                                 WHERE Offer_Status__c = 'LOGIN REQUESTED'
                                 ];
   }                             
                             
}