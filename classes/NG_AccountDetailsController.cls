public with sharing class NG_AccountDetailsController 
{ //test demo Swayam
 //giving demo
    public Account account{get;set;}
    
    public NG_AccountDetailsController(Apexpages.StandardController stdcontroller)
    {
        this.account = (Account)stdcontroller.getrecord();
      /* this.account = [Select id,Name,   
                             Nature_of_Business__c,
                             Tender_Stage__c,
                             Pending_With__c,
                             Registered_Number__c,
                             Registered_Address_Line_1__c,
                             Registered_Address_Line_2__c,
                             Registered_Address_Line_3__c,
                             Commercial_Contact_Name__c,
                             Dispatch_Email_Address__c,
                             Offer_Stage__c,
                             Offer_Status__c
                             FROM Account
                             where id =: ApexPages.currentPage().getparameters.get('id')];
      */
      }
    
    public PageReference approve()
    {   
        account.Offer_Status__c = 'LOGIN APPROVED';
        update account;
        
        Contact tempcontact = new Contact();
        tempcontact = [Select id,
                              name,
                              Email 
                              From Contact
                              Where accountid =: account.id];
                              
        EmailTemplate emailtemp = new EmailTemplate();
        emailTemp = [Select id from EmailTemplate where DeveloperName = 'ContactApprovedTemplate'];
                              
        Messaging.SingleEmailMessage mssg = new Messaging.SingleEmailMessage();
        mssg.setTemplateId(emailTemp.id);
        //mssg.setWhatId(tempcontact.id);
        mssg.setTargetObjectId(tempcontact.id);
        mssg.setSenderDisplayName('National Grid - Helpdesk');
        mssg.setReplyTo('team.natioalgrid@ng.com');
        
        Messaging.sendEmail(new Messaging.SingleEmailMessage[]{mssg});                  
        
        system.debug('@@@@Inside Email');
        
        PageReference pageref = new PageReference('/apex/NG_HomeViewerPage');
        return pageref;
    }
    
    
    public PageReference reject()
    {
        account.Offer_Status__c = 'LOGIN REJECTED';
        update account;
        
        PageReference pageref = new PageReference('/apex/NG_HomeViewerPage');
        return pageref;
    }
    
    
}